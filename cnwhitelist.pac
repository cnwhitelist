//=============================================
// PROXY WHITELIST FOR CHINESE WEBSITE
//---------------------------------------------
// Maintained by Tunghsiao Liu (sparanoid.com)
//---------------------------------------------
// Add this .pac file to your system proxy
// settings under internet configuration. You 
// may also need to change your DNS.
// If you have problem with local pac on Lion, 
// try to fire up a localhost like this:
// http://localhost:8888/cnwhitelist/cnwhitelist.pac
//=============================================

// Variables
var direct = direct;

// Change your proxy here. Just FYI You can add 
// multiple failback proxies.
//---------------------------------------------
var proxy = "SOCKS5 127.0.0.1:3333; SOCKS 127.0.0.1:3333; DIRECT";

function FindProxyForURL(url, host) {

	// Bypass Local Networks
	if (isPlainHostName(host)) {
		return direct;
	}

	// Known IPs
	var resolved_ip = dnsResolve(host);

	if (isInNet(resolved_ip, "10.0.0.0", "255.0.0.0") ||
		isInNet(resolved_ip, "172.16.0.0",  "255.240.0.0") ||
		isInNet(resolved_ip, "192.168.0.0", "255.255.0.0") ||
		isInNet(resolved_ip, "127.0.0.0", "255.255.255.0")) {
		return direct;
	}

	// Known Domains
	var whitelist = [
		// Have to use proxy or it dosen"t work.
		//---------------------------------------------
		"*://ip138.com/*",
		"*://*.ip138.com/*",

		// Assume all .cn domains use Chienses servers.
		// So no need to re-add specified domains.
		//---------------------------------------------
		"*://*.cn/*",

		// Web Portals / News / Emails
		//---------------------------------------------
		"*://163.com/*",
		"*://*.163.com/*",

			"*://netease.com/*",
			"*://*.netease.com/*",

			"*://126.net/*",
			"*://*.126.net/*",

		"*://126.com/*",
		"*://*.126.com/*",

		"*://yeah.net/*",
		"*://*.yeah.net/*",

		"*://sohu.com/*",
		"*://*.sohu.com/*",

			"*://sogou.com/*",
			"*://*.sogou.com/*",

		"*://qq.com/*",
		"*://*.qq.com/*",

		"*://sdo.com/*",
		"*://*.sdo.com/*",

			"*://staticsdo.com/*",
			"*://*.staticsdo.com/*",

		// Weibos
		//---------------------------------------------
		"*://weibo.com/*",
		"*://*.weibo.com/*",

		// Search Engines
		//---------------------------------------------
		"*://zdic.net/*",
		"*://*.zdic.net/*",

		"*://baidu.com/*",
		"*://*.baidu.com/*",

			"*://bdimg.com/*",
			"*://*.bdimg.com/*",

		"*://cidianwang.com/*",
		"*://*.cidianwang.com/*",

		// Social Networks
		//---------------------------------------------
		"*://renren.com/*",
		"*://*.renren.com/*",

		"*://douban.com/*",
		"*://*.douban.com/*",

		"*://zhihu.com/*",
		"*://*.zhihu.com/*",

			"*://yupoo.com/*",
			"*://*.yupoo.com/*",

		"*://weiphone.com/*",
		"*://*.weiphone.com/*",

		"*://pcbeta.com/*",
		"*://*.pcbeta.com/*",

		"*://moko.cc/*",
		"*://*.moko.cc/*",

			"*://moko.hk/*",
			"*://*.moko.hk/*",

		// Shopping Sites
		//---------------------------------------------
		"*://taobao.com/*",
		"*://*.taobao.com/*",

			"*://taobaocdn.com/*",
			"*://*.taobaocdn.com/*",

			"*://atpanel.com/*",
			"*://*.atpanel.com/*",

		"*://tmall.com/*",
		"*://*.tmall.com/*",

		"*://alipay.com/*",
		"*://*.alipay.com/*",

		"*://alibaba.com/*",
		"*://*.alibaba.com/*",

			"*://aliimg.com/*",
			"*://*.aliimg.com/*",

		"*://360buy.com/*",
		"*://*.360buy.com/*",

			"*://360buyimg.com/*",
			"*://*.360buyimg.com/*",

		"*://eachnet.com/*",
		"*://*.eachnet.com/*",

		"*://icson.com/*",
		"*://*.icson.com/*",

		"*://yihaodian.com/*",
		"*://*.yihaodian.com/*",

			"*://yihaodianimg.com",
			"*://*.yihaodianimg.com",

		"*://vancl.com/*",
		"*://*.vancl.com/*",

			"*://vanclimg.com",
			"*://*.vanclimg.com",

		// Banks
		//---------------------------------------------
		"*://cmbchina.com/*",
		"*://*.cmbchina.com/*",

			"*://bankofchina.com/*",
			"*://*.bankofchina.com/*",

		"*://ccb.com/*",
		"*://*.ccb.com/*",

		// Telecommunications
		//---------------------------------------------
		"*://10010.com/*",
		"*://*.10010.com/*",

		"*://ct10000.com/*",
		"*://*.ct10000.com/*",

		// Express Delivery
		//---------------------------------------------
		"*://sf-express.com/*",
		"*://*.sf-express.com/*",

		// ACG
		//---------------------------------------------
		"*://ra.gg/*",
		"*://*.ra.gg/*",

			"*://mukkyu.com/*",
			"*://*.mukkyu.com/*",

		"*://jsharer.com/*",
		"*://*.jsharer.com/*",

		"*://moeid.com/*",
		"*://*.moeid.com/*",

		"*://030buy.com/*",
		"*://*.030buy.com/*",

		"*://saraba1st.com/*",
		"*://*.saraba1st.com/*",

		"*://ktxp.com/*",
		"*://*.ktxp.com/*",

		// Other
		//---------------------------------------------
		"*://feidee.com/*",
		"*://*.feidee.com/*",

		// Webmaster / Counter
		//---------------------------------------------
		"*://51.la/*",
		"*://*.51.la/*",

		"*://cnzz.com/*",
		"*://*.cnzz.com/*",

		"*://google-analytics.com/*",
		"*://*.google-analytics.com/*",

		"*://jiankongbao.com/*",
		"*://*.jiankongbao.com/*",

		"*://frantech.ca/*",
		"*://*.frantech.ca/*",

		// Temporary / Debug
		//---------------------------------------------
		// Google Music
		"*://clients.google.com/*",
		"*://*.clients.google.com/*",

		// Powered by Tunghsiao Liu
		//---------------------------------------------
		"*://drrr.us/*",
		"*://*.drrr.us/*",

		"*://nio2.com/*",
		"*://*.nio2.com/*",

		"*://sparanoid.com/*",
		"*://*.sparanoid.com/*"
	];

	for (var n=0; n < whitelist.length; n++) {
		if (shExpMatch(url, whitelist[n])) {
			return direct;
		}
	}

	return proxy;
}

// China is dangerous, Lets" go to Mars.