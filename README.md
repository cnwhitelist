cnwhitelist
===========

A whitelist to bypass proxy settings for Chinese website.

Note
------------

* You may need to change your DNS to Google Public DNS or alternatives.
* You may also change your proxy port if you don't like mine.

How to Contribute?
------------------

* Fork it and download it to your Mac.
* Edit, remember to put your sites in right categories.
* Send me Pull Request.

Format
------

Let's assume a website is accessed by `sparanoid.com`, then we use the following rule to bypass the proxy:

	'sparanoid.com/*',

And it can be also accessed via `www.sparanoid.com`, and it has a blog in subdomain `log.sparanoid.com`, then we use:

	'sparanoid.com/*',
	'*.sparanoid.com/*',

Oh, I forgot, it also has FTP access: `ftp://sparanoid.com/` and it uses HTTPS for WordPress dashboard like `https://log.sparanoid.com/wp-admin/`, so now we modify them to:

	'*://sparanoid.com/*'
	'*://*.sparanoid.com/*'

For convenience, I just treated all domains as the last situation I described above.


License
-------

(The MIT License)

Copyright (c) 2011 Tunghsiao Liu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[jk]: https://github.com/mojombo/jekyll
[sp]: http://sparanoid.com/